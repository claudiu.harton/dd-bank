const controllers = {
  reset: require("./reset"),
  auth: require("./auth"),
  services: require("./services"),
  requests: require("./requests"),
  middleware: require("./middleware"),
};

module.exports = controllers;
