const express = require("express");
const router = express.Router();
const { requests } = require("../controllers");

//reqeusts
router.post("/", requests.createRequest);
router.delete("/", requests.deleteRequest);
router.put("/", requests.editRequest);
router.get("/", experiences.getRequests);

router.put("/check", requests.checkRequest);

router.get("/all", requests.getAllRequests);

module.exports = router;
