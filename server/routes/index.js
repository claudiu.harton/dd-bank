const express = require("express");
const router = express.Router();
const { reset, middleware } = require("../controllers");

const authRouter = require("./auth")
const experiencesRouter = require("./experiences")

//reset
router.get("/reset", reset.reset);

router.use("/", authRouter);
router.use("/experiences", middleware.checkLogin, experiencesRouter);

module.exports = router;
