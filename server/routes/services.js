const express = require("express");
const router = express.Router();
const { services } = require("../controllers");

router.get("/all", services.getAllServices);
//employee
router.post("/", services.addService);
router.delete("/", services.deleteService);
router.put("/", services.editService);
router.put("/account", services.updateAccount);

// client
router.get("/", services.getUserServices);
router.post("/extern", services.externalTransfer);
router.post("/intern", services.internalTransfer);
router.post("/receive", services.receiveMoney);

module.exports = router;
