export default {
  services: [
    {
      id: 1,
      currency: "RON",
      interest: 15,
      serviceType: "Prima casa",
      administrationFee: 10,
      processFee: 10,
      dae: 10
    },
    {
      id: 2,
      currency: "RON",
      interest: 20,
      serviceType: "Nevoi personale",
      administrationFee: 10,

      processFee: 11,
      dae: 15
    }
  ],
  clientServices: [
    {
      id: 1,
      totalValue: 12000,
      remainingValue: 10000,
      remainingMonths: 12,
      service: {
        id: 1,
        currency: "RON",
        interest: 15,
        serviceType: "Prima casa",
        administrationFee: 10,
        processFee: 10,
        dae: 10
      },
      id: 1,
      nextPayment: 835,
      currency: "RON",
      interest: 15,
      serviceType: "Prima casa",
      administrationFee: 10,
      processFee: 10,
      dae: 10
    }
  ],
  appointments: [],
  appointmentsAsAdmin: [],
  medicAppointments: [],
  patientAppointments: []
};
