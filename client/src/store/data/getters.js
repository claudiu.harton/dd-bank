export function getServices(state) {
  return state.services;
}
export function getClientServices(state) {
  return state.clientServices;
}
export function getAppointments(state) {
  return state.appointments;
}

export function getMedicAppointments(state) {
  return state.medicAppointments;
}

export function getPatientAppointments(state) {
  return state.patientAppointments;
}

export function getAdminAppointments(state) {
  return state.appointmentsAsAdmin;
}
